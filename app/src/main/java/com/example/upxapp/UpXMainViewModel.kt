package com.example.upxapp

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.upxapp.ui.screens.Screens
import com.example.upxapp.util.CurrentAppData
import com.example.upxapp.util.Navigator
import com.example.upxapp.util.UpXServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class UpXMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = UpXServerClient.create()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    private fun switchToApp() {
        Navigator.navigateTo(Screens.MAIN_SCREEN)
    }
}