package com.example.upxapp.util

import com.example.upxapp.ui.screens.Screens
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

object Navigator {
    private val mNavigationFlow = MutableStateFlow(Screens.SPLASH_SCREEN)
    val navigationFlow = mNavigationFlow.asStateFlow()

    fun navigateTo(targetScreens: Screens) {
        mNavigationFlow.tryEmit(targetScreens)
    }
}