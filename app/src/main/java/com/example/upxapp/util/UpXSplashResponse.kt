package com.example.upxapp.util

import androidx.annotation.Keep

@Keep
data class UpXSplashResponse(val url : String)