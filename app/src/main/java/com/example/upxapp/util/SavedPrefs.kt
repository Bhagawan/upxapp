package com.example.upxapp.util

import android.content.Context
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("UpX", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

        fun saveBalance(context: Context) = context
            .getSharedPreferences("UpX", Context.MODE_PRIVATE)
            .edit()
            .putInt("balance", CurrentAppData.totalBalance)
            .apply()

        fun getBalance(context: Context): Int {
            val shP = context.getSharedPreferences("UpX", Context.MODE_PRIVATE)
            return shP.getInt("balance", 0)
        }
    }
}