package com.example.upxapp

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts.*
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.example.upxapp.ui.theme.UpXAppTheme
import com.example.upxapp.util.CurrentAppData
import com.example.upxapp.util.NavigationComponent
import com.example.upxapp.util.SavedPrefs


class MainActivity : ComponentActivity() {
    private lateinit var filePathCallback: ValueCallback<Array<Uri>>

    var mLauncher = registerForActivityResult(
        StartActivityForResult()
    ) {
        if(it.resultCode == RESULT_OK) {
            it.data?.let { intent ->
                intent.data?.let { uri ->
                    filePathCallback.onReceiveValue(arrayOf(uri))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = ViewModelProvider(this)[UpXMainViewModel::class.java]
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, SavedPrefs.getId(this))
        CurrentAppData.totalBalance = SavedPrefs.getBalance(this)
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            UpXAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationComponent(navController, webView, ::fixScreen )
                }
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private  fun fixScreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }



    @SuppressLint("SetJavaScriptEnabled")
    private fun createWebView() : WebView {
        return WebView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            webViewClient = WebViewClient()
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }

                override fun onShowFileChooser(view: WebView, filePath: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                    filePathCallback = filePath
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    i.putExtra(Intent.EXTRA_TITLE, "File Chooser")
                    mLauncher.launch(i)
                    return true
                }
            }
            settings.domStorageEnabled = true
            settings.allowContentAccess = true
            settings.allowFileAccess = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            settings.loadWithOverviewMode = true
            settings.useWideViewPort = true
            settings.javaScriptEnabled = true
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }
}