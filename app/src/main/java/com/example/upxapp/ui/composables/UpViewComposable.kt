package com.example.upxapp.ui.composables

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.viewinterop.AndroidView
import com.example.upxapp.ui.view.UpView

@Composable
fun UpViewComposable(gameInterface: UpView.UpViewInterface, modifier: Modifier = Modifier) {
    AndroidView(factory = { UpView(it) },
        modifier = modifier.clipToBounds(),
        update = {
            it.setInterface(gameInterface)
        })
}