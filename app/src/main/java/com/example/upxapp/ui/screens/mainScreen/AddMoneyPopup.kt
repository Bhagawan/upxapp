package com.example.upxapp.ui.screens.mainScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.R
import com.example.upxapp.ui.composables.AddButton
import com.example.upxapp.ui.composables.TextFieldInterface
import com.example.upxapp.ui.composables.TextFieldWithButtons
import com.example.upxapp.ui.theme.Green
import com.example.upxapp.ui.theme.Grey
import com.example.upxapp.ui.theme.Transparent_black
import com.example.upxapp.util.CurrentAppData
import com.example.upxapp.util.SavedPrefs

@Composable
fun AddMoneyPopup(onHide: () -> Unit) {
    var a = 0
    val context = LocalContext.current
    BoxWithConstraints(modifier = Modifier
        .fillMaxSize()
        .clickable { onHide() }
        .background(Transparent_black), contentAlignment = Alignment.Center) {
        val w = maxWidth
        Column(modifier = Modifier
            .wrapContentSize()
            .background(Grey, RoundedCornerShape(10.dp))
            .border(2.dp, Green, shape = RoundedCornerShape(10.dp))
            .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
            Text(stringResource(id = R.string.add_amount),
                fontSize = 15.sp,
                textAlign = TextAlign.Center,
                color =  Color.White)
            TextFieldWithButtons(inputInterface = object : TextFieldInterface
            {
                override fun onAmountSet(amount: Int) {
                    a = amount
                }

                override fun onAmountSet(amount: Float) {}

            }, int = true, modifier = Modifier.width(w / 2.0f))
            AddButton(Modifier.width(w / 2.0f).height(30.dp)) {
                CurrentAppData.totalBalance += a
                SavedPrefs.saveBalance(context)
                onHide()
            }
        }
    }
}