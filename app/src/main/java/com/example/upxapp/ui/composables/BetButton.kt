package com.example.upxapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.ui.theme.Blue_light
import com.example.upxapp.ui.theme.Grey_light

@Composable
fun BetButton(text: String, shape: Shape = RectangleShape, modifier: Modifier = Modifier, onClick: () -> Unit) {
    BoxWithConstraints(modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        val brush = Brush.linearGradient(
            0.0f to Color(143, 158, 170, 255),
            0.1f to Blue_light,
            0.9f to Blue_light,
            1.0f to Color(143, 158, 170, 255),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value))
        Box(modifier = modifier.fillMaxWidth()
                .background(brush, shape)
                .padding(5.dp)
                .clip(RoundedCornerShape(5.dp))
                .clickable { onClick() }, contentAlignment = Alignment.Center) {

            Text(
                text,
                fontSize = 15.sp,
                color = Color.White,
                style = TextStyle(shadow = Shadow(Grey_light, offset = Offset(2.0f, 2.0f))),
                textAlign = TextAlign.Center)
        }
    }
}
