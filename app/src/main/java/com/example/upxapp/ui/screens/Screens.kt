package com.example.upxapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    MAIN_SCREEN("main"),
    SERVER_ERROR_SCREEN("server_error"),
    GAME_SCREEN("game")
}