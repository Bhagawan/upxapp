package com.example.upxapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.R
import com.example.upxapp.ui.theme.Grey_light

@Composable
fun AddButton(modifier: Modifier = Modifier, onPress: () -> Unit) {

    BoxWithConstraints(modifier = modifier.clickable { onPress() }) {
        val brush = Brush.linearGradient(
            0.0f to Color(0, 159, 98, 255),
            0.7f to Color(0, 159, 98, 100),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        Text(
            stringResource(id = R.string.btn_add),
            fontSize = 20.sp,
            color = Color.White,
            style = TextStyle(shadow = Shadow(Grey_light, offset = Offset(2.0f, 2.0f))),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
                .background(brush, RoundedCornerShape(20.dp))
                .clip(RoundedCornerShape(20.dp)))
    }
}