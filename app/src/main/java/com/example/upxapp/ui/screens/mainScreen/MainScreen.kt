package com.example.upxapp.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.upxapp.R
import com.example.upxapp.ui.composables.*
import com.example.upxapp.ui.theme.*
import com.example.upxapp.ui.view.UpView
import com.example.upxapp.util.CurrentAppData
import com.example.upxapp.util.SavedPrefs
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun MainScreen() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val balance by viewModel.balance.collectAsState()
    val switchState by viewModel.switch.collectAsState()
    val focusManager = LocalFocusManager.current
    val betTaken by viewModel.betTaken.collectAsState()
    val gameActive by viewModel.gameActive.collectAsState()
    val betAmount by viewModel.betAmount.collectAsState()
    val context = LocalContext.current
    val localDensity = LocalDensity.current

    val history by viewModel.history.collectAsState()

    var bottomPanelHeight by remember { mutableStateOf(0.dp)}
    LaunchedEffect(key1 = "1") {
        viewModel.balance.onEach {
            CurrentAppData.totalBalance = it
            SavedPrefs.saveBalance(context)
        }.launchIn(viewModel.viewModelScope)
    }

    Column(modifier = Modifier
        .fillMaxSize()
        .clickable(MutableInteractionSource(), null) { focusManager.clearFocus() }
        .background(Blue_dark)
        .padding(5.dp)) {
        Box(modifier = Modifier
            .padding(bottom = 1.dp)
            .background(Transparent_white, RoundedCornerShape(5.dp)), contentAlignment = Alignment.Center) {
            Text(text = "$ $balance",
                fontSize = 12.sp,
                textAlign = TextAlign.Center,
                color =  Color.Yellow,
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.Center))
            Image(painterResource(id = R.drawable.ic_baseline_add_box_24),
                contentDescription = null,
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .clickable(MutableInteractionSource(), null) { viewModel.showAddMoneyPopup() })
        }
        BoxWithConstraints(modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f, true), contentAlignment = Alignment.Center) {
            val height = maxHeight
            Row(modifier = Modifier
                .fillMaxWidth()
                .height(height), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(3.dp)) {
                AndroidView(factory = { UpView(it) },
                    modifier = Modifier
                        .weight(9.0f, true)
                        //.aspectRatio(1.0f)
                        .clipToBounds(),
                    update = { view ->
                        view.setInterface(object : UpView.UpViewInterface {
                            override fun stop(endCoefficient: Float) {
                                viewModel.result(endCoefficient)
                            }

                            override fun updateCoefficient(coefficient: Float) {
                                viewModel.updateCoefficient(coefficient)
                            }
                        })
                        viewModel.startGame.onEach {
                            if(it) view.startGame()
                        }.launchIn(viewModel.viewModelScope)
                    })

                History(history, modifier = Modifier.height(height))
            }
        }

        Box(modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .onGloballyPositioned {
                bottomPanelHeight = with(localDensity) { it.size.height.toDp() }
            }) {
            Column(modifier = Modifier
                .background(Transparent_white, RoundedCornerShape(10.dp))
                .padding(10.dp),
                verticalArrangement = Arrangement.spacedBy(5.dp),
                horizontalAlignment = Alignment.CenterHorizontally) {

                Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                    Column(modifier = Modifier.weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                        Box(modifier = Modifier
                            .fillMaxWidth()
                            .height(30.dp), contentAlignment = Alignment.Center) {
                            Text(
                                stringResource(id = R.string.bet),
                                fontSize = 10.sp,
                                textAlign = TextAlign.Center,
                                color =  Color.White)
                        }

                        TextFieldWithButtons(inputInterface = object : TextFieldInterface
                        {
                            override fun onAmountSet(amount: Int) {
                                viewModel.setBetAmount(amount)
                            }
                            override fun onAmountSet(amount: Float) {}
                        }, int = true, currentBet = betAmount, maxAmount = balance)
                    }
                    Column(modifier = Modifier.weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .height(30.dp), verticalAlignment = Alignment.CenterVertically) {
                            Text(
                                stringResource(id = R.string.take_on),
                                fontSize = 10.sp,
                                textAlign = TextAlign.Center,
                                color =  Color.White, modifier = Modifier.weight(1.0f, true))
                            Switch(checked = switchState, onCheckedChange = { viewModel.checkSwitch(it) }, colors = SwitchDefaults.colors(
                                checkedTrackColor = Green,
                                uncheckedTrackColor = Grey_light,
                                checkedThumbColor = Color.White,
                                uncheckedThumbColor = Color.White
                            ), interactionSource = remember { MutableInteractionSource() },
                                modifier = Modifier
                                    .scale(0.5f)
                                    .height(30.dp))
                        }
                        TextFieldWithButtons(inputInterface = object : TextFieldInterface
                        {
                            override fun onAmountSet(amount: Int) {}
                            override fun onAmountSet(amount: Float) {
                                viewModel.setAutoBetCoefficient(amount)
                            }
                        }, int = false, minAmount = 1.0f)
                    }
                }
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(1.dp)) {
                    BetButton(text = stringResource(id = R.string.btn_min),
                        RoundedCornerShape(topStart = 5.dp, bottomStart = 5.dp),
                        modifier = Modifier.weight(1.0f, true)) {
                        viewModel.setBetAmount(10)
                    }
                    BetButton(text = stringResource(id = R.string.btn_x2),
                        modifier = Modifier.weight(1.0f, true)) {
                        viewModel.setBetAmount(betAmount * 2)
                    }
                    BetButton(text = stringResource(id = R.string.btn_half),
                        modifier = Modifier.weight(1.0f, true)) {
                        viewModel.setBetAmount(betAmount / 2)
                    }
                    BetButton(text = stringResource(id = R.string.btn_allin),
                        RoundedCornerShape(topEnd = 5.dp, bottomEnd = 5.dp),
                        modifier = Modifier.weight(1.0f, true)) {
                        viewModel.setBetAmount(balance)
                    }
                }
                //Spacer(modifier = Modifier.weight(1.0f, true))
                StartButton(gameActive && !betTaken,
                    Modifier
                        .fillMaxWidth().height(50.dp)
                        .padding(horizontal = 50.dp), action = { viewModel.pressPlayButton() })
                //Spacer(modifier = Modifier.weight(1.0f, true))
            }
            if(betTaken) {
                focusManager.clearFocus()
                Box(modifier = Modifier
                    .height(bottomPanelHeight)
                    .fillMaxWidth()
                    .background(Transparent_black, RoundedCornerShape(10.dp))
                    .clickable(
                        MutableInteractionSource(), null
                    ) { }, contentAlignment = Alignment.Center) {
                    Text(stringResource(id = R.string.bet_taken),
                        fontSize = 30.sp,
                        textAlign = TextAlign.Center,
                        color =  Color.White)
                }
            }
        }
    }

    val addMoneyPopup by viewModel.addMoneyPopup.collectAsState()
    if(addMoneyPopup) AddMoneyPopup {
        viewModel.hideAddMoneyPopup()
        viewModel.updateBalance()
    }
}