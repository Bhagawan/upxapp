package com.example.upxapp.ui.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.R
import com.example.upxapp.ui.theme.Grey_light

@Composable
fun History(history: List<Float>, modifier: Modifier = Modifier) {
    val elementHeight = 30
    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
        Text(stringResource(id = R.string.history), textAlign = TextAlign.Center, fontSize = 10.sp, color = Grey_light)
        BoxWithConstraints(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
            val height = maxHeight
            val amount = maxHeight.value.toInt() / elementHeight
            if(history.isNotEmpty()) Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp), modifier = Modifier.height(height)) {
                for (n in 0..amount.coerceAtMost(history.size - 1)) {
                    Text("${"%.2f".format(history[history.size - 1 - n])} x", textAlign = TextAlign.Center, fontSize = 10.sp, color = Color.White)
                }
            }
        }
    }
}