package com.example.upxapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.R
import com.example.upxapp.ui.theme.Grey_light

@Composable
fun StartButton(gameStarted: Boolean = true, modifier: Modifier = Modifier, action: () -> Unit = {}) {
    BoxWithConstraints(modifier = modifier.clip(RoundedCornerShape(10.dp)).clickable { action() }) {
        val brushPause = Brush.linearGradient(
            0.0f to Color(0, 159, 98, 255),
            0.7f to Color(0, 159, 98, 100),
            1.0f to Color(0, 159, 98, 100),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        val brushActive = Brush.linearGradient(
            0.0f to Color(134, 97, 68, 255),
            0.7f to Color(134, 97, 68, 100),
            1.0f to Color(134, 97, 68, 100),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        Text(
            stringResource(id = if(!gameStarted) R.string.btn_play else R.string.btn_take_bet),
            fontSize = 30.sp,
            color = Color.White,
            style = TextStyle(shadow = Shadow(Grey_light, offset = Offset(2.0f, 2.0f))),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth().background(if(!gameStarted) brushPause else brushActive, RoundedCornerShape(10.dp)).clip(RoundedCornerShape(10.dp)))
    }

}