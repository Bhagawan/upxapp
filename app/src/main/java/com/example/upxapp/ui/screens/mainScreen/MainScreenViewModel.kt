package com.example.upxapp.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.upxapp.util.CurrentAppData
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainScreenViewModel: ViewModel() {
    private val _balance = MutableStateFlow(CurrentAppData.totalBalance)
    val balance = _balance.asStateFlow()

    private val _addMoneyPopup = MutableStateFlow(false)
    val addMoneyPopup = _addMoneyPopup.asStateFlow()

    private val _betTaken = MutableStateFlow(false)
    val betTaken = _betTaken.asStateFlow()

    private val _gameActive = MutableStateFlow(false)
    val gameActive = _gameActive.asStateFlow()

    private val _startGame = MutableSharedFlow<Boolean>()
    val startGame = _startGame.asSharedFlow()

    private val _switch = MutableStateFlow(false)
    val switch = _switch.asStateFlow()

    private val _betAmount = MutableStateFlow(0)
    val betAmount = _betAmount.asStateFlow()

    private val historyList = ArrayList<Float>()
    private val _history = MutableStateFlow(historyList.toList())
    val history = _history.asStateFlow()

    private var autoBet = 1.0f
    private var currentCoeff = 1.0f

    fun showAddMoneyPopup() {
        _addMoneyPopup.tryEmit(true)
    }

    fun hideAddMoneyPopup() {
        _addMoneyPopup.tryEmit(false)
    }

    fun setBetAmount(amount: Int) {
        _betAmount.tryEmit(amount)
       // betAmount = amount
    }

    fun setAutoBetCoefficient(coefficient: Float) {
        autoBet = coefficient
    }

    fun result(finalCoefficient: Float) {
        _gameActive.tryEmit(false)
        _betTaken.tryEmit(false)
        historyList.add(finalCoefficient)
        _history.tryEmit(historyList.toList())
    }

    fun updateCoefficient(coefficient: Float) {
        currentCoeff = coefficient
        if(gameActive.value && !betTaken.value && switch.value) {
            if(autoBet <= coefficient) {
                _betTaken.tryEmit(true)
                _balance.tryEmit((balance.value + betAmount.value * autoBet).toInt())
            }
        }
    }

    fun pressPlayButton() {
        if(gameActive.value) {
            _betTaken.tryEmit(true)
            _balance.tryEmit((balance.value + betAmount.value * currentCoeff).toInt())
        } else {
            currentCoeff = 1.0f
            viewModelScope.launch { _startGame.emit(true) }
            _gameActive.tryEmit(true)
            _balance.tryEmit(balance.value - betAmount.value)
        }
    }

    fun updateBalance() {
        _balance.tryEmit(CurrentAppData.totalBalance)
    }

    fun checkSwitch(state: Boolean) = _switch.tryEmit(state)
}