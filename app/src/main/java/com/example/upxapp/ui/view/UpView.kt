package com.example.upxapp.ui.view

import android.content.Context
import android.graphics.*
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.upxapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class UpView (context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var padLeft = 10.0f
    private var padBottom = 10.0f
    private var oneSize = 100.0f
    private val speed = 0.5f // in second

    private var betTime = 3.0f
    private var betCoefficient = 1.0f
    private var coeffHistory = ArrayList<Float>()
    private var resultTimer = 0.0f

    private val pauseBitmap = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_pause_24)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)

    companion object {
        const val STATE_BET = 0
        const val STATE_GAME = 1
        const val STATE_RESULT = 2
        const val STATE_PAUSE = 3
    }

    private var state = STATE_PAUSE
    private var pausedState = state

    private var mInterface: UpViewInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            padLeft = mWidth / 30.0f
            padBottom = padLeft
            oneSize = (mHeight - padBottom) / 4.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawBackground(it)
            when(state) {
                STATE_BET -> {
                    updateBetTimer()
                    drawBetTimer(it)
                }
                STATE_GAME -> {
                    updateCoefficient()
                    updateSize()
                    drawGame(it)
                    drawCoefficient(it, ResourcesCompat.getColor(context.resources, R.color.white, null))
                }
                STATE_RESULT -> {
                    drawGame(it)
                    updateResultTimer()
                    drawCoefficient(it, ResourcesCompat.getColor(context.resources, R.color.gold, null))
                }
                STATE_PAUSE -> {
                    drawGame(it)
                    //drawPause(it)
                }
            }
        }
    }

    /// Public

    fun setInterface(i: UpViewInterface) {
        mInterface = i
    }

    fun startGame() {
        if(state == STATE_PAUSE || state == STATE_RESULT) restart()
    }

    fun pause() {
        if(state != STATE_PAUSE) {
            pausedState = state
            state = STATE_PAUSE
        }
    }

    fun resume() {
        state = pausedState
    }

    //// Private

    private fun drawBackground(c: Canvas) {
        c.drawColor(ResourcesCompat.getColor(context.resources, R.color.blue_dark, null))
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.textSize = 10.0f
        val secondLen = (mWidth - padLeft) * 0.9f / (coeffHistory.size / 60.0f).coerceAtLeast(2.0f )
        val grey = ResourcesCompat.getColor(context.resources, R.color.grey_transparent, null)
        var tX = padLeft
        var sec = 0
        while (tX < mWidth) {
            p.color = Color.WHITE
            c.drawText(sec.toString() + "s", tX, mHeight - padBottom / 2, p)
            p.color = grey
            c.drawLine(tX, 0.0f, tX, mHeight - padBottom, p)
            sec++
            tX += secondLen
        }
        var tY = mHeight - padBottom
        var coeff = 1.0f
        while (tY > 0) {
            p.color = Color.WHITE
            c.drawText(coeff.toString() + "x", padLeft / 2.0f, tY, p)
            p.color = grey
            c.drawLine(padLeft, tY, mWidth.toFloat(), tY, p)
            coeff++
            tY -= oneSize
        }
    }

    private fun updateBetTimer() {
        betTime -= 1.0f / 60.0f
        if(betTime <= 1.5) {
            betTime = 3.0f
            state = STATE_GAME
        }
    }

    private fun drawBetTimer(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.textSize = 100.0f
        p.textAlign = Paint.Align.CENTER
        c.drawText(betTime.toInt().toString(), mWidth / 2.0f, mHeight / 2.0f, p)
    }

    private fun updateCoefficient() {
        betCoefficient += (speed / 60.0f) * betCoefficient
        mInterface?.updateCoefficient(betCoefficient)
        coeffHistory.add(betCoefficient)
        val end = Random.nextFloat()
        if(end > 0.99f) {
            mInterface?.stop(betCoefficient)
            state = STATE_RESULT
        }
    }

    private fun updateSize() {
        oneSize = (mHeight - padBottom) * 0.9f / betCoefficient .coerceAtLeast(4.0f)
    }

    private fun drawGame(c: Canvas) {
        val p = Paint()
        p.textSize = 20.0f
        p.textAlign = Paint.Align.CENTER
        p.color = ResourcesCompat.getColor(context.resources, R.color.red, null)
        p.strokeWidth = 2.0f
        val horizontalStep = (mWidth - padLeft) * 0.9f / coeffHistory.size.coerceAtLeast(120 )
        val line = Path().apply {
            moveTo(padLeft, mHeight - padBottom)
            for(coeff in coeffHistory.withIndex()) {
                lineTo(padLeft + coeff.index * horizontalStep, mHeight - padBottom - (coeff.value - 1) * oneSize)
                if(coeff.index > 0) {
                    c.drawLine(padLeft + (coeff.index - 1) * horizontalStep, mHeight - padBottom - (coeffHistory[coeff.index - 1] - 1) * oneSize,
                        padLeft + coeff.index * horizontalStep, mHeight - padBottom - (coeff.value - 1) * oneSize, p)
                }
            }
        }

        if(coeffHistory.isNotEmpty()) {
            line.lineTo((coeffHistory.size - 1) * horizontalStep + padLeft, mHeight - padBottom)
            line.close()
        }
        p.color = ResourcesCompat.getColor(context.resources, R.color.red_transparent, null)
        p.style = Paint.Style.FILL
        c.drawPath(line, p)

        p.color = ResourcesCompat.getColor(context.resources, R.color.red, null)
        c.drawCircle((coeffHistory.size - 1) * horizontalStep + padLeft, mHeight - padBottom - (betCoefficient - 1) * oneSize, 3.0f, p)
    }

    private fun updateResultTimer() {
        resultTimer += (1.0f / 60.0f)
        if(resultTimer > 2) {
            resultTimer = 0.0f
            state = STATE_PAUSE
//            restart()
//            mInterface?.start()
        }
    }

    private  fun drawCoefficient(c: Canvas, color: Int) {
        val p = Paint()
        p.color = color
        p.textSize = 100.0f
        p.textAlign = Paint.Align.CENTER
        c.drawText("%.2f".format(betCoefficient) + "x", mWidth / 2.0f, mHeight / 2.0f, p)
    }

    private fun drawPause(c: Canvas) {
        val size = mWidth / 6.0f
        c.drawBitmap(pauseBitmap, null, Rect((mWidth / 2.0f - size).toInt(), (mHeight / 2 - size).toInt(), (mWidth / 2.0f + size).toInt(), (mHeight / 2 + size).toInt()), Paint())
    }

    private fun restart() {
        oneSize = (mHeight - padBottom) / 4.0f
        state = STATE_BET
        coeffHistory.clear()
        coeffHistory.add(1.0f)
        betCoefficient = 1.0f
        betTime = 3.0f
        updateSize()
    }

    interface UpViewInterface {
        fun stop(endCoefficient: Float)
        fun updateCoefficient(coefficient: Float)
    }
}