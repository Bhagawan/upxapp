package com.example.upxapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.upxapp.R
import com.example.upxapp.ui.theme.Grey_light

@Composable
fun TextFieldWithButtons(inputInterface: TextFieldInterface, int: Boolean, currentBet: Int? = null, maxAmount: Int? = null, minAmount : Float = 0.0f, endLIneDecor: String = "", modifier: Modifier = Modifier) {
    var amount by remember { mutableStateOf(minAmount)}
    maxAmount?.let {
        if(amount > it) amount = it.toFloat()
    }
    currentBet?.let {
        if(int && currentBet != amount.toInt()) amount = currentBet.toFloat()
    }
    val focusManager = LocalFocusManager.current
    Row(modifier = modifier
        .background(Color.White, shape = RoundedCornerShape(10.dp))
        .height(intrinsicSize = IntrinsicSize.Max)) {
        Image(
            painterResource(id = R.drawable.ic_baseline_remove_24),
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .clickable {
                    amount = (amount - 1).coerceAtLeast(0.0f)
                    if (int) inputInterface.onAmountSet(amount.toInt())
                    else inputInterface.onAmountSet(amount)
                }
                .background(Grey_light, RoundedCornerShape(topStart = 10.dp, bottomStart = 10.dp)))
        BasicTextField(
            value = if(int) amount.coerceAtLeast(minAmount).toInt().toString() else amount.coerceAtLeast(minAmount).toString(),
            onValueChange = {
                try {
                    val a = it. toFloat()
                    if(maxAmount != null) {
                        if(a <= maxAmount && a >= minAmount) amount = a
                    } else amount = it.toFloat().coerceAtLeast(minAmount)
                    if(int) inputInterface.onAmountSet(amount.toInt())
                    else inputInterface.onAmountSet(amount)
                } catch (e: Exception) { }
            },
            singleLine = true,
            textStyle = TextStyle(fontSize = 14.sp, color = Grey_light, textAlign = TextAlign.Center),
            modifier = Modifier.weight(3.0f, true),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            keyboardActions = KeyboardActions(onSearch = {
                focusManager.clearFocus()
            }),
            decorationBox = { innerTextField ->
                Row(
                    modifier = Modifier
                        .background(color = Color.White)
                        .padding(1.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                        innerTextField()
                        Text(endLIneDecor, color = Grey_light, fontSize = 10.sp, modifier = Modifier.align(Alignment.CenterEnd))
                    }
                }
            }
        )
        Image(
            painterResource(id = R.drawable.ic_baseline_add_24),
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .clickable {
                    if (maxAmount != null) {
                        if (amount + 1 <= maxAmount) amount++
                    } else amount++
                    if (int) inputInterface.onAmountSet(amount.toInt())
                    else inputInterface.onAmountSet(amount)
                }
                .background(Grey_light, RoundedCornerShape(topEnd = 10.dp, bottomEnd = 10.dp)))

    }
}

interface TextFieldInterface {
    fun onAmountSet(amount: Int)
    fun onAmountSet(amount: Float)
}